import React from 'react'
import {Link} from 'react-router-dom'

function header() {
  return (
    <div className="shadow">
      <div className="headline clipped">Marc Timperley</div>
      <div className="nav">
        <Link to='/'>Home</Link>
        <Link to='/projects'>Projects</Link>
      </div>
  </div>)
}

export default header
