import React from 'react'

function home () {
  return (<div>
    <ul className="specialisation">
      <li className="tooltip">JavaScript
        <span className="tooltiptext">developer of</span>
        <span className="tooltiptextright">using vanilla JS or React, jQuery, etc.</span>
      </li>
      <li className="tooltip">Node.js
        <span className="tooltiptextright">developer and administrator</span>
      </li>
      <li className="tooltip">Application Design
        <span className="tooltiptext">full lifecycle</span>
        <span className="tooltiptextright">of apps large and small</span>
      </li>
      <li className="tooltip">Lead Software Engineer
        <span className="tooltiptext">agile</span>
        <span className="tooltiptextright">mangaging high performing teams</span>
      </li>
    </ul>
  </div>)
}
export default home
