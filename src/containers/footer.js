import React from 'react'

function footer() {
  return (<div className="footer">
  <div className="shadow">
    &copy; 2020 Marc Timperley
    <a className="App-link" href="https://linkedin.com/in/marctimperley" target="_blank" rel="noopener noreferrer">
      <img className="badge small" src={require("../images/linkedin.svg")} alt="LinkedIn profile link" />
    </a>
    <a className="App-link" href="https://gitlab.com/MarcTimperley" target="_blank" rel="noopener noreferrer">
      <img className="badge small" src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-stacked-rgb.svg" alt="GitLab profile link" />
    </a>
  </div>
</div>)
}

export default footer
