import React from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group' // ES6
import '../carousel.css'

let orcaImg = require('../images/orca.png')
let grafanaImg = require('../images/grafana.png')
let fileScanImg = require('../images/file_scan.png')
let docImage = require('../images/doc.png')
class Gallery extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [
        orcaImg,
        fileScanImg,
        docImage,
        grafanaImg
      ],
      captions: [
        'SOx Analysis and Compliance Reporting (Node.js + REST + bootstrap)',
        'Remote Web Server Change Monitoring (Node.js + bootstrap)',
        'Documentation example for tools module (JsDoc + Boxy template)',
        'Service Performance Monitoring (TICK stack + grafana)'
      ],
      current: 0,
      isNext: true
    }
    this.handlerPrev = this.handlerPrev.bind(this)
    this.handlerNext = this.handlerNext.bind(this)
    this.goToHistoryClick = this.goToHistoryClick.bind(this)
  }

  handlerPrev() {
    let index = this.state.current,
        length = this.state.items.length
    if( index < 1 ) index = length
        index = index - 1
    this.setState({
      current: index,
      isNext: false
    })
  }

  handlerNext() {
    let index = this.state.current, length = this.state.items.length - 1
    if( index === length ) index = -1
    index = index + 1
    this.setState({
      current: index,
      isNext: true
    })
  }

  goToHistoryClick( curIndex, index ) {
    let next = (curIndex < index)
    this.setState({
      current: index,
      isNext: next
    })
  }

  render(){
    let index = this.state.current,
        isnext = this.state.isNext,
        src = this.state.items[index],
        caption = this.state.captions[index]
    return (
      <div className="app">
        <div className="carousel">
         <ReactCSSTransitionGroup
            transitionName={{
            enter: isnext ? 'enter-next' : 'enter-prev',
            enterActive: 'enter-active',
            leave: 'leave',
            leaveActive: isnext ? 'leave-active-next' : 'leave-active-prev'
          }}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
           >
             <div className="carousel_slide" key={index}>
               <img src={src} alt=""/>
             </div>
           </ReactCSSTransitionGroup>
           <button className="carousel_control carousel_control__prev" onClick={this.handlerPrev}><span></span></button>
           <button className="carousel_control carousel_control__next" onClick={this.handlerNext}><span></span></button>
          <div className="carousel_history">
            <History
              current={this.state.current}
              items={this.state.items}
              changeSilde={this.goToHistoryClick}
            />
          </div>
          <div className="carousel_text">
            {caption}
          </div>
          </div>
      </div>
    )
  }
}

class History extends React.Component {

  render() {
    let current = this.props.current
    let items = this.props.items.map( (el, index) => {
      let name = (index === current) ? 'active' : ''
      return (
        <li key={index}>
          <button
            className={name}
            onClick={ () => this.props.changeSilde(current, index) }
          ></button>
        </li>
      )
    })

    return (
      <ul>{items}</ul>
    )
  }
}

export default Gallery
