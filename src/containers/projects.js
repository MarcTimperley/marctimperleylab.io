import React from 'react';

function projects () {
  return (
    <div className="cards">

      <a target="_blank" rel="noopener noreferrer" title="Performance measurement dashboard (Nodejs + Bootstrap)" href="https://github.com/MarcTimperley/response-dashboard" className="project-tile project-link">
        <div className="project-title">Performance Management Dashboard</div>
        <img src={require('../images/dashboard.png')} rel="noopener noreferrer" alt="Performance Dashboard" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="ExpressJS Boilerplate for fast API development (Express + Node.js)" href="https://gitlab.com/MarcTimperley/eplate" className="project-tile project-link">
        <div className="project-title">ePlate</div>
        <img src={require('../images/eplate.png')} rel="noopener noreferrer" alt="ePlate" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="Professional profile deployed as an SPA (React)" href="https://marctimperley.gitlab.io" className="project-tile project-link">
        <div className="project-title">React Profile Page</div>
        <img src={require('../images/profile1.png')} rel="noopener noreferrer" alt="Profile (React)" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="Mobile Health tracking application using chartjs (Bootstrap + chartjs)" href="https://gitlab.com/MarcTimperley/healthtrack" className="project-tile project-link">
        <div className="project-title">Mobile Health Tracker</div>
        <img src={require('../images/healthtrack.png')} rel="noopener noreferrer" alt="Health Tracker" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="Covid-19 Map (d3js)" href="http://web-mt.s3-website.eu-west-2.amazonaws.com/covid_map.html" className="project-tile project-link">
        <div className="project-title">Covid-19 Map</div>
        <img src={require('../images/covid_map.png')} rel="noopener noreferrer" alt="Covid-19 Map" className="tile-image" />
      </a><a target="_blank" rel="noopener noreferrer" title="Explore images with CSS transforms (Vanilla JS)" href="https://gitlab.com/MarcTimperley/carousel" className="project-tile project-link">
        <div className="project-title">Carousel</div>
        <img src={require('../images/carousel.png')} rel="noopener noreferrer" alt="Carousel" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="Simple CRUD front end in React (React)" href="https://gitlab.com/MarcTimperley/crud-simple" className="project-tile project-link">
        <div className="project-title">CRUD React</div>
        <img src={require('../images/crud-simple.png')} alt="Simple CRUD app" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="Get times for any direct train service in the UK (Bootstrap + REST API + Node.js)" href="https://gitlab.com/MarcTimperley/train" className="project-tile project-link">
        <div className="project-title">Train Times</div>
        <img src={require('../images/train.png')} alt="Train times app" className="tile-image" />
      </a>
      <a target="_blank" rel="noopener noreferrer" title="Displays css named colours and their variants (React)" href="https://gitlab.com/MarcTimperley/cssnames" className="project-tile project-link">
        <div className="project-title">CSS Palette</div>
        <img src={require('../images/cssnames.png')} alt="CSS Palette App" className="tile-image" />
      </a>
    </div>
  )
}
export default projects;
