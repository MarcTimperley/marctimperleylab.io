import React, {Component} from 'react'

// import Gallery from './containers/gallery'
import Projects from './containers/projects'
import Header from './containers/header'
import Footer from './containers/footer'
import Home from './containers/home'
import createBrowserHistory from './utils/history'

import {Router, Route, Switch} from 'react-router'

class Routes extends Component {
  render() {
    return (
      <div>
      <Router history={createBrowserHistory}>
          <div className="header">
            <Header/>
          </div>
          <div className="content">
          <Switch>
            <Route exact={true} path="/" render={() => <Home/>}/>
            <Route path="/projects" render={() => <Projects/>}/>
          </Switch>
          </div>
          <Footer/>
      </Router>
    </div>
  )
}
}

export default Routes
